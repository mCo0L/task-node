#!/usr/bin/env node

let amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function (error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function (error1, channel) {
        if (error1) {
            throw error1;
        }

        // Queue Name
        let queue = 'hello';

        channel.assertQueue(queue, {
            durable: false
        });

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

        // Redis connection creation
        let redis = require('redis');
        let client = redis.createClient();
        client.on('connect', function () {
            console.log('connected redis');
        });
        let mysql = require('mysql');


        // Mysql Connection Creation
        let mysqlCon = mysql.createConnection({
            host: "localhost",
            user: "local_user",
            password: "test@123",
            database: "test_db"
        });
        mysqlCon.connect(function (err) {
            if (err) throw err;
            console.log("Connected! to mysql");
        });


        // Channel Consumer - listens on channel for any new message
        channel.consume(queue, function (msg) {
            let key = msg.content.toString();
            // console log for monitoring what's happening
            console.log(" [x] Received Id: %s",key);

            // if entry exists in redis for the key
            client.get(key, function (err, reply) {
                console.log(reply);
                // delete record from redis
                client.del(key);

                // insert record in mysql
                let sql = "INSERT INTO entries ( title ) VALUES ?";
                let VALUES = [[reply]];
                mysqlCon.query(sql, [VALUES], function (err, result) {
                    if (err) throw err;
                    console.log("1 record inserted");
                });
            });

        }, {
            noAck: true
        });
    });
});
