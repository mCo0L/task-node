const express = require('express');
const app = express();
app.use(express.json());


// Mysql Connection Creation
let mysql = require('mysql');

let mysqlCon = mysql.createConnection({
    host: "localhost",
    user: "local_user",
    password: "test@123",
    database: "test_db"
});
mysqlCon.connect(function (err) {
    if (err) throw err;
    console.log("Connected! to mysql");
});

// RAW sql for fetching all db rows data from entries table
let sql = "SELECT * FROM  entries";

//READ Request Handlers
app.get('/', (req, res) => {
    res.send('Welcome to mukul\'s REST API with Node.js !!');
});


// Required Post API to load all data from db
app.get('/api/get', (req, res) => {
    mysqlCon.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

//PORT ENVIRONMENT VARIABLE
const port = process.env.PORT || 8080;
app.listen(port, () => console.log(`Listening on port ${port}..`));
